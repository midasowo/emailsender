# EmailSender

## About project

A microservice responsible for sending e-mails to users.

It allows you to store email addresses to which you can propagate a message sent from an external source.
The external source is a txt file. It provides a REST API that allows CRUD operations on users' email addresses.

## Used tech stack
* Spring Boot 2.7.8
* Java 17
* H2 database


