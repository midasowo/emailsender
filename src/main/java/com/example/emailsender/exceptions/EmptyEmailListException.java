package com.example.emailsender.exceptions;

public class EmptyEmailListException extends RuntimeException {
    public EmptyEmailListException(String message) {
        super(message);
    }
}
