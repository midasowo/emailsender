package com.example.emailsender.exceptions;

public class NoMessageToSendException extends RuntimeException {
    public NoMessageToSendException(String message) {
        super(message);
    }
}
