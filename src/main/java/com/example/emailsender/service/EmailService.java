package com.example.emailsender.service;

import com.example.emailsender.model.Email;
import com.example.emailsender.exceptions.EmptyEmailListException;
import com.example.emailsender.exceptions.NoMessageToSendException;
import com.example.emailsender.repository.EmailRepository;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmailService {

    private final JavaMailSender mailSender;
    private final EmailRepository repo;

    public EmailService(JavaMailSender mailSender, EmailRepository repo) {
        this.mailSender = mailSender;
        this.repo = repo;
    }

    public void addEmail(Email email) {
        repo.save(email);
    }

    public Optional<Email> editAddress(Email newEmail) {
        Optional<Email> byUserName = getUserByName(newEmail.getUsername());
        if (byUserName.isPresent()) {
            byUserName.get().setAddress(newEmail.getAddress());
            Email save = repo.save(byUserName.get());
            return Optional.of(save);
        } else {
            return Optional.empty();
        }
    }

    public Optional<Email> editUser(Email newEmail) {
        Optional<Email> byAddress = getEmailByAddress(newEmail.getAddress());
        if (byAddress.isPresent()) {
            byAddress.get().setUsername(newEmail.getUsername());
            Email save = repo.save(byAddress.get());
            return Optional.of(save);
        } else {
            return Optional.empty();
        }
    }

    public Email updateEmailByFields(Long id, Map<String, Object> fields) {
        Optional<Email> existingEmail = repo.findById(id);
        if (existingEmail.isPresent()) {
            fields.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(Email.class, key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, existingEmail.get(), value);
            });
            return repo.save(existingEmail.get());
        }
        return null;
    }

    public Optional<Email> removeEmailByAddress(Email email) {
        Optional<Email> byAddress = getEmailByAddress(email.getAddress());
        if (byAddress.isPresent()) {
            repo.delete(byAddress.get());
            return byAddress;
        } else {
            return Optional.empty();
        }
    }

    public void removeEmailById(Long id) {
        repo.deleteById(id);
    }

    public List<Email> getEmailDataList() {
        return repo.findAll();
    }

    public List<String> getEmailsToSend() {
        return repo.findAll().stream().map(Email::getAddress).toList();
    }

    public Optional<Email> getEmailByAddress(String address) {
        return repo.findByAddress(address);
    }

    public Optional<Email> getUserByName(String name) {
        return repo.findByUsername(name);
    }

    public Optional<Email> getEmailById(Long id) {
        return repo.findById(id);
    }

    public void sendEmail(String subject) {
        if (getEmailsToSend().isEmpty()) {
            throw new EmptyEmailListException("Email list is empty");
        } else {
            SimpleMailMessage message = new SimpleMailMessage();
            try {
                List<String> text = Files.readAllLines(Path.of("message.txt"));
                String bodyToSend = text.stream().map(String::valueOf).collect(Collectors.joining());
                for (int i = 0; i < getEmailsToSend().size(); i++) {
                    message.setTo(getEmailsToSend().get(i));
                    message.setSubject(subject);
                    message.setText(bodyToSend);
                    mailSender.send(message);
                }
            } catch (MailException | IOException ex) {
                throw new NoMessageToSendException("Couldn't find message to send");
            }
        }
    }

    /*public void sendEmail(String subject) {
        SimpleMailMessage message = new SimpleMailMessage();
        try {
            List<String> body = Files.readAllLines(Path.of("message.txt"));
            String bodyToSend = body.stream().map(String::valueOf).collect(Collectors.joining());
            message.setBcc(getEmailsToSend());
            message.setSubject(subject);
            message.setText(bodyToSend);
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        try {
            mailSender.send(message);
        }
        catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
    }*/


}
