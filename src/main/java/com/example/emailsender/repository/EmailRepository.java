package com.example.emailsender.repository;

import com.example.emailsender.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmailRepository extends JpaRepository<Email, Long> {

    Optional<Email> findByAddress(String address);
    Optional<Email> findByUsername(String username);
}
