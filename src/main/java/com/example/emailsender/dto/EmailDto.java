package com.example.emailsender.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public record EmailDto(@NotEmpty
                       @NotNull
                       @Email(message = "Write proper email address")
                       String address,
                       @NotNull(message = "Title cannot be null")
                       @NotEmpty(message = "Title cannot be empty")
                       @Length(
                               min = 3,
                               max = 200,
                               message = "Username cannot be shorter than 3 and longer than 200"
                       )
                       String username
) {
}

