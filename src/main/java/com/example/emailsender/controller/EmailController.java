package com.example.emailsender.controller;

import com.example.emailsender.dto.EmailDto;
import com.example.emailsender.model.Email;
import com.example.emailsender.service.EmailService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/emails")
public class EmailController {

    private final EmailService service;

    public EmailController(EmailService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<List<Email>> getEmails() {
        return new ResponseEntity<>(service.getEmailDataList(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Email> addEmail(@RequestBody @Valid EmailDto emailDto) {
        Email email = new Email();
        BeanUtils.copyProperties(emailDto, email);
        service.addEmail(email);
        return new ResponseEntity<>(email, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public Email updateEmailByFields(@PathVariable Long id,@RequestBody Map<String, Object> fields){
        return service.updateEmailByFields(id,fields);
    }

    @DeleteMapping("/address")
    public ResponseEntity<Email> removeEmailByAddress(@RequestBody @Valid EmailDto emailDto) {
        Email email = new Email();
        BeanUtils.copyProperties(emailDto, email);
        return ResponseEntity.of(service.removeEmailByAddress(email));
    }

    @DeleteMapping("/address/{id}")
    public ResponseEntity removeEmailById(@PathVariable("id") Long id) {
        service.removeEmailById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/message")
    public ResponseEntity sendEmail() {
        service.sendEmail("Message");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
