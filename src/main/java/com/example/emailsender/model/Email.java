package com.example.emailsender.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "emails")
public class Email {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String address;
    private String username;

    public Email() {
    }

    public Email(Long id, String address, String username) {
        this.id = id;
        this.address = address;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String ownerName) {
        this.username = ownerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(id, email.id) && Objects.equals(address, email.address) && Objects.equals(username, email.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, address, username);
    }

    @Override
    public String toString() {
        return "EmailModel{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", ownerName='" + username + '\'' +
                '}';
    }
}
