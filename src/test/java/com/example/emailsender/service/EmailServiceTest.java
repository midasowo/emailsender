package com.example.emailsender.service;

import com.example.emailsender.exceptions.EmptyEmailListException;
import com.example.emailsender.model.Email;
import com.example.emailsender.repository.EmailRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private EmailRepository emailRepositoryMock;

    @InjectMocks
    private EmailService emailServiceMock;

    @Test
    void shouldGetEmailDataList() {
        // given
        Mockito.when(emailRepositoryMock.findAll()).thenReturn(Arrays.asList(
                new Email(1L, "test1@example", "Test1"),
                new Email(2L, "test2@example", "Test2")
        ));
        // when
        List<Email> emailDataList = emailServiceMock.getEmailDataList();
        // then
        assertThat(emailDataList).hasSize(2);
    }

    @Test
    void shouldGetEmptyEmailDataList() {
        // when
        List<Email> emailDataList = emailServiceMock.getEmailDataList();
        // then
//        assertThat(emailDataList).isEmpty();
        Assertions.assertEquals(List.of(), emailDataList);
    }

    @Test
    void shouldGetEmailsToSend() {
        // given
        Mockito.when(emailRepositoryMock.findAll()).thenReturn(Arrays.asList(
                new Email(1L, "test1@example", "Test1"),
                new Email(2L, "test2@example", "Test2"),
                new Email(3L, "test3@example", "Test3")
        ));
        // when
        List<String> emailsToSend = emailServiceMock.getEmailsToSend();
        // then
//        assertThat(emailsToSend).hasSize(3);
//        assertThat(emailsToSend).containsOnly("test1@example", "test2@example", "test3@example");
        Assertions.assertEquals(emailsToSend, List.of("test1@example", "test2@example", "test3@example"));
    }

    @Test
    void shouldAddEmail() {
        // given
        Email email = new Email(1L, "test1@example", "Test1");
        Email save = emailRepositoryMock.save(email);
        // when
        emailServiceMock.addEmail(save);
        // then
//        assertThat(email.getAddress()).isEqualTo("test1@example");
        Assertions.assertEquals("test1@example", email.getAddress());
    }


}